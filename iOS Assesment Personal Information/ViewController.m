//
//  ViewController.m
//  iOS Assesment Personal Information
//
//  Created by click labs 115 on 10/8/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *imgStatusBar;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollPI;
@property (strong, nonatomic) IBOutlet UIImageView *imgScroll;
@property (strong, nonatomic) IBOutlet UIImageView *imgPersonal;
@property (strong, nonatomic) IBOutlet UITextField *txtFirstName;
@property (strong, nonatomic) IBOutlet UITextField *txtLastName;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtPhoneNumber;

@property (nonatomic, strong) IBOutlet UILabel *lblName;

@end

@implementation ViewController
@synthesize scrollPI;
//@synthesize txtFirstName;

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    _imgPersonal.layer.cornerRadius = _imgPersonal.frame.size.width/2;
    _imgPersonal.clipsToBounds = YES;
    _txtFirstName.layer.borderWidth = 1.30f;
    _txtFirstName.layer.borderColor = [UIColor colorWithRed:20/255.0 green:55/255.0 blue:85/255.0 alpha:1].CGColor;
    _txtFirstName.delegate = self;
    _txtLastName.layer.borderWidth = 1.30f;
    _txtLastName.layer.borderColor = [UIColor colorWithRed:20/255.0 green:55/255.0 blue:85/255.0 alpha:1].CGColor;
    _txtLastName.delegate = self;
    
    
    _txtEmail.layer.borderWidth = 1.30f;
    _txtEmail.layer.borderColor = [UIColor colorWithRed:20/255.0 green:55/255.0 blue:85/255.0 alpha:1].CGColor;
    _txtEmail.delegate = self;
    
    _txtPhoneNumber.layer.borderWidth = 1.30f;
    _txtPhoneNumber.layer.borderColor = [UIColor colorWithRed:20/255.0 green:55/255.0 blue:85/255.0 alpha:1].CGColor;
    _txtPhoneNumber.delegate = self;
    
    // Do any additional setup after loading the view, typically from a nib.
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.lblName.bounds];
    self.lblName.layer.masksToBounds = NO;
    self.lblName.layer.shadowColor = [UIColor blackColor].CGColor;
    self.lblName.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    self.lblName.layer.shadowOpacity = 0.5f;
    self.lblName.layer.shadowPath = shadowPath.CGPath;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}



-(void)dismissKeyboard {
    [_txtFirstName resignFirstResponder];
    [_txtLastName resignFirstResponder];
    [_txtEmail resignFirstResponder];
    [_txtPhoneNumber resignFirstResponder];
}

- (void)keyboardWillShow:(NSNotification *)sender
{
    CGSize kbSize = [[[sender userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
        [scrollPI setContentInset:edgeInsets];
        [scrollPI setScrollIndicatorInsets:edgeInsets];
    }];
}

- (void)keyboardWillHide:(NSNotification *)sender
{
    NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsZero;
        [scrollPI setContentInset:edgeInsets];
        [scrollPI setScrollIndicatorInsets:edgeInsets];
    }];
}



- (IBAction)pressed:(id)sender  {
    
    if ([self checkSpecialCharecterForFirstName : _txtFirstName.text  ] == YES ) {
        [self alertShowFirstNameAndSeconedName];
    }
    
    else if ([self checkSpecialCharecterForLastName : _txtFirstName.text  ] == YES){
        
        [self alertShowFirstNameAndSeconedName];
    }
    else if ([_txtFirstName.text length] == 0){
        [self AllFieldMandatory];
        
    }
    else if ([_txtLastName.text length] == 0){
        [self AllFieldMandatory];
    }
    else if ([_txtEmail.text length] == 0){
        [self AllFieldMandatory];
    }
    else if ([_txtPhoneNumber.text length] == 0){
        [self AllFieldMandatory];
    }
    else if ([self emailVAlidation : _txtEmail.text ] == NO){
        [self emailAlert];
    }
    else if ([self phonenumbervalidation : _txtPhoneNumber.text] == YES){
        [self phoneNumberAlert];
    }
    else if ([self phoneNumberLength : _txtPhoneNumber.text] == YES ) {
        [self phoneNumberAlert];
    }
    else {
        [self updatesuceesfully];
        _txtFirstName.text = @"";
        _txtLastName.text = @"";
        _txtEmail.text= @"";
        _txtPhoneNumber.text = @"";
    }
}
-(BOOL) checkSpecialCharecterForFirstName : (NSString *) text{
    
    
    NSCharacterSet *set =  [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"]invertedSet];
    if ([text rangeOfCharacterFromSet:set].location == NSNotFound){
        NSLog(@"No Special Charecter");
        return NO;
    }
    else{
        NSLog(@"Has Special CHarecter");
        return YES;
    }
}


-(BOOL) checkSpecialCharecterForLastName : (NSString *) text{
    
    
    NSCharacterSet *set =  [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"]invertedSet];
    if ([text rangeOfCharacterFromSet:set].location == NSNotFound){
        NSLog(@"No Special Charecter");
        return NO;
    }
    else{
        NSLog(@"Has Special CHarecter");
        return YES;
    }
}


- (void)alertShowFirstNameAndSeconedName{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@" Alert"
                                                                   message:@"Only Alphabets Are Allowed :"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)AllFieldMandatory{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@" Alert"
                                                                   message:@"All Field Are Mandatory :"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (BOOL) emailVAlidation : (NSString *) sender {
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    //Valid email address
    
    if ([emailTest evaluateWithObject:_txtEmail.text] == YES)
    {
        return YES;
    }
    else
    {
        NSLog(@"email not in proper format");
        return NO;
    }
}

- (void)emailAlert{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@" Alert In Email :"
                                                                   message:@"Please Enter Valid Email :"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (void)PasswordLength {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Alert In Password" message:@"Your Max Password Length 10" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (void)passwordMissMatchAlert{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@" Alert In ConfirmPassword :"
                                                                   message:@"PassWord Match Not Found :"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

- (BOOL)phoneNumberLength : (NSString *) sender {
    
    if ([_txtPhoneNumber.text length ] == 10) {
        return NO;
    }
    else{
        NSLog(@"Enter Valid Phn Number");
        return YES;
    }
    
    
}


-(BOOL) phonenumbervalidation : (NSString *) text{
    
    
    NSCharacterSet *set =  [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"]invertedSet];
    if ([text rangeOfCharacterFromSet:set].location == NSNotFound){
        NSLog(@"No Special Charecter");
        return NO;
    }
    else{
        NSLog(@"Has Special CHarecter");
        return YES;
    }
}

- (void)phoneNumberAlert{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@" Alert In Email :"
                                                                   message:@"Please Enter Valid 10 Digits PhoneNumber :"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)updatesuceesfully{
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@" Sucessfull :"
                                                                   message:@"Profile Update Sucessfully :"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
